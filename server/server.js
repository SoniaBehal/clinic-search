import express from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';
import routes from './routes/index';
import fs from 'fs';
import path from 'path';

const app = express();
dotenv.config();

// create a write stream (in append mode)
const accessLogStream = fs.createWriteStream(path.join(__dirname, '../access.log'), { flags: 'a' })

// setup the logger
app.use(morgan('tiny', { stream: accessLogStream }))

app.use("/", routes);

const port = process.env.port || 4000;

app.listen(port, (err) => {
    if (err) { console.log("Error in starting server ") }
    else { console.log("Server started at ", port); }
});
